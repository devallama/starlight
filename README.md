![Starlight logo](https://gitlab.com/devallama/starlight/-/raw/master/title.png)

# Starlight Theme

Starlight dark theme created by devallama.

Submit recommendations for changes on the issues board [here](https://gitlab.com/devallama/starlight/-/issues).

![VSCode in the Starlight theme](https://gitlab.com/devallama/starlight/-/raw/master/vscode.png)

## GitLab repository

[GitLab](https://gitlab.com/devallama/starlight)
