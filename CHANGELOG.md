# Change Log

All notable changes to the "starlight" extension will be documented in this file.

## [1.0.2]

- Update Git repository in package.json

## [1.0.1]

- Resize icon

## [1.0.0]

- Initial release
